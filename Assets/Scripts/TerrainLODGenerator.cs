﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TerrainLODGenerator : MonoBehaviour
{
    //public static string sourcePath = "TerrainPrefab/Terrain";
    public static string sourcePath = "Assets/Prefabs/TerrainPrefab/Terrain.prefab";
    public static string destPath = "Assets/Prefabs/TerrainClone/";



    // Start is called before the first frame update
    void Start()
    {
        CreateTerrainLODPrefab(sourcePath, destPath);
    }


    /// <summary>
    /// Creates copy of prefab containing Terrain component from given source path
    /// and saves it to given destination path.
    /// </summary>
    /// <param name="prefabPath">path of source prefab</param>
    /// <param name="pathToSave">path to save new prefab</param>
    public void CreateTerrainLODPrefab(string prefabPath, string pathToSave)
    {
        if (EditorApplication.isPlaying)
        {
            Debug.LogError("Editor run script only! Load prefab using Resource.Load() file path.");
            return;
        }
    
        GameObject terrainPrefab = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject)) as GameObject;
        print(terrainPrefab);
        TerrainData terrainData = terrainPrefab.GetComponent<Terrain>().terrainData;
     
        TerrainData clonedTerrainData = CloneTerrainData(terrainData);
        GameObject terrainPrefabClone = Terrain.CreateTerrainGameObject(clonedTerrainData);

        terrainPrefabClone.GetComponent<Terrain>().heightmapPixelError = terrainPrefab.GetComponent<Terrain>().heightmapPixelError * 2;
       // terrainPrefabClone.GetComponent<Terrain>().Flush();
        terrainPrefabClone.name += "LOD";

       //saving new terrainData
        AssetDatabase.CreateAsset(clonedTerrainData, pathToSave + terrainPrefabClone.name + ".asset");
       
        //saving new prefab
        string prefabLODPath = pathToSave + terrainPrefabClone.name + ".prefab";
        prefabLODPath = AssetDatabase.GenerateUniqueAssetPath(prefabLODPath);
        PrefabUtility.SaveAsPrefabAssetAndConnect(terrainPrefabClone, prefabLODPath, InteractionMode.UserAction);

        AssetDatabase.SaveAssets();
    }

    private TerrainData CloneTerrainData(TerrainData source)
    {
        TerrainData clone = new TerrainData();

        clone.alphamapResolution = source.alphamapResolution / 2;
        clone.baseMapResolution = source.baseMapResolution / 2;
        clone.SetDetailResolution(source.detailResolution / 2, 16);

        clone.heightmapResolution = source.heightmapResolution;
        clone.size = source.size;

        clone.splatPrototypes = CloneSplatPrototypes(source.splatPrototypes);

        //clone.terrainLayers = new TerrainLayer[source.terrainLayers.Length];
       // source.terrainLayers.CopyTo(clone.terrainLayers, 0);

        clone.SetAlphamaps(0, 0, source.GetAlphamaps(0, 0, clone.alphamapWidth, clone.alphamapHeight));
        clone.SetHeights(0, 0, source.GetHeights(0, 0, clone.heightmapResolution, clone.heightmapResolution));

        for (int n = 0; n < source.detailPrototypes.Length; n++)
        {
            clone.SetDetailLayer(0, 0, n, source.GetDetailLayer(0, 0, source.detailWidth, source.detailHeight, n));
        }

        clone.RefreshPrototypes();

        return clone;
    }

    SplatPrototype[] CloneSplatPrototypes(SplatPrototype[] original)
    {
        SplatPrototype[] splatDup = new SplatPrototype[original.Length];

        for (int n = 0; n < splatDup.Length; n++)
        {
            splatDup[n] = new SplatPrototype
            {
                metallic = original[n].metallic,
                normalMap = original[n].normalMap,
                smoothness = original[n].smoothness,
                specular = original[n].specular,
                texture = original[n].texture,
                tileOffset = original[n].tileOffset,
                tileSize = original[n].tileSize
            };
        }

        return splatDup;
    }
}
