﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TerrainLODGenerator))]
public class TerrainCopyEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TerrainLODGenerator myScript = (TerrainLODGenerator)target;
        if (GUILayout.Button("Generate Terrain LOD"))
        {
            myScript.CreateTerrainLODPrefab(TerrainLODGenerator.sourcePath, TerrainLODGenerator.destPath);
        }
    }
}
